package com.example.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlin.math.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        task3(4.0, 6.0)
        findZ(3.5, 3.5, 7.3)
        task4(3, 5, 6)
        task0(13, 5, 6)
        println(task3())

        println(findZ(3.5,3.5,7.3))
        println(task6(3,1))
        println(task6(-4,-1))
        println(task6(1,1))
        println(task6(2,2))
        println(task6(-2,5))
    }

    fun findZ(a: Double, b: Double, c: Double): Double = ((a - 3) * b / 2) + c



    fun task3(x: Double = 1.0, y: Double = 4.0): Double {
        return (sin(x) + cos(y) / cos(x) - sin(y)) * tan(x * y)
    }

    fun task4(a: Int, b: Int, c: Int) {
        if ((a + b) <= c) {
            println("Does not exist")
        } else if ((a + c) <= b) {
            println("Does not exist")
        } else if ((b + c) <= a) {
            println("Does not exist")
        } else {
            println("exist")
        }
    }

    fun task0(a: Int, b: Int, c: Int) {
        when {
            c >= a + b || a >= c + b || b >= c + a -> {
                println("Does not exist")
            }
        }
    }
}


fun task6(x: Int, y: Int): Boolean {
    when {
        x > 2 || y > 4 || x < -2 || y > 4 || x < -4 || y < -3 || x > 4 || y < -3 -> {
            return false
        }
        else -> {
            return true
        }

    }
}